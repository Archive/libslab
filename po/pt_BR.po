# Brazilian Portuguese translation of gnome-main-menu.
# Copyright (C) 2006-2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-main-menu package.
# Novell Language <language@novell.com>, 2006.
# Amadeu A. Barbosa Junior <amadeu@gmail.com>, 2007.
# Pedro de Medeiros <pedrovmm@gmail.com>, 2007.
# Leonardo Ferreira Fontenelle <leo.fontenelle@gmail.com>, 2007.
# Og Maciel <ogmaciel@gnome.org>, 2008.
# Fábio Nogueira <deb-user-ba@ubuntu.com>, 2008.
# Michel Recondo <mrecondo@gmail.com>, 2008, 2011.
# Flamarion Jorge <jorge.flamarion@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-main-menu\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"main-menu&component=libslab\n"
"POT-Creation-Date: 2010-12-11 00:24+0000\n"
"PO-Revision-Date: 2011-01-14 11:00-0200\n"
"Last-Translator: Flamarion Jorge <jorge.flamarion@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. make start action
#: ../libslab/application-tile.c:372
#, c-format
msgid "Start %s"
msgstr "Iniciar %s"

#: ../libslab/application-tile.c:393
msgid "Help"
msgstr "Ajuda"

#: ../libslab/application-tile.c:440
msgid "Upgrade"
msgstr "Atualizar"

#: ../libslab/application-tile.c:455
msgid "Uninstall"
msgstr "Desinstalar"

#: ../libslab/application-tile.c:782 ../libslab/document-tile.c:738
msgid "Remove from Favorites"
msgstr "Remover dos favoritos"

#: ../libslab/application-tile.c:784 ../libslab/document-tile.c:740
msgid "Add to Favorites"
msgstr "Adicionar aos favoritos"

#: ../libslab/application-tile.c:869
msgid "Remove from Startup Programs"
msgstr "Remover dos programas de inicialização"

#: ../libslab/application-tile.c:871
msgid "Add to Startup Programs"
msgstr "Adicionar aos programas de inicialização"

#: ../libslab/app-shell.c:756
#, c-format
msgid "Your filter \"%s\" does not match any items."
msgstr "Seu filtro \"%s\" não casa com nenhum dos itens."

#: ../libslab/app-shell.c:758
msgid "No matches found."
msgstr "Nenhum resultado encontrado."

#: ../libslab/app-shell.c:911
msgid "Other"
msgstr "Outro"

#: ../libslab/bookmark-agent.c:1110
msgid "New Spreadsheet"
msgstr "Nova planilha"

#: ../libslab/bookmark-agent.c:1115
msgid "New Document"
msgstr "Novo documento"

#: ../libslab/bookmark-agent.c:1168
msgctxt "Home folder"
msgid "Home"
msgstr "Home"

#: ../libslab/bookmark-agent.c:1175
msgid "Documents"
msgstr "Documentos"

#: ../libslab/bookmark-agent.c:1183
msgid "Desktop"
msgstr "Área de trabalho"

#: ../libslab/bookmark-agent.c:1190
msgid "File System"
msgstr "Sistema de arquivos"

#: ../libslab/bookmark-agent.c:1194
msgid "Network Servers"
msgstr "Servidores de rede"

#: ../libslab/bookmark-agent.c:1223
msgid "Search"
msgstr "Pesquisar"

#. make open with default action
#: ../libslab/directory-tile.c:169
#, c-format
msgid "Open"
msgstr "Abrir"

#. make rename action
#: ../libslab/directory-tile.c:188 ../libslab/document-tile.c:235
msgid "Rename..."
msgstr "Renomear..."

#: ../libslab/directory-tile.c:202 ../libslab/directory-tile.c:211
#: ../libslab/document-tile.c:249 ../libslab/document-tile.c:258
msgid "Send To..."
msgstr "Enviar para..."

#. make move to trash action
#: ../libslab/directory-tile.c:226 ../libslab/document-tile.c:284
msgid "Move to Trash"
msgstr "Mover para a lixeira"

#: ../libslab/directory-tile.c:236 ../libslab/directory-tile.c:453
#: ../libslab/document-tile.c:294 ../libslab/document-tile.c:854
msgid "Delete"
msgstr "Excluir"

#: ../libslab/directory-tile.c:529 ../libslab/document-tile.c:1016
#, c-format
msgid "Are you sure you want to permanently delete \"%s\"?"
msgstr "Você tem certeza de que deseja excluir permanentemente \"%s\"?"

#: ../libslab/directory-tile.c:530 ../libslab/document-tile.c:1017
msgid "If you delete an item, it is permanently lost."
msgstr "Se você excluir um item, ele será perdido permanentemente."

#: ../libslab/document-tile.c:194
#, c-format
msgid "Open with \"%s\""
msgstr "Abrir com \"%s\""

#: ../libslab/document-tile.c:208
msgid "Open with Default Application"
msgstr "Abrir com a aplicação padrão"

#: ../libslab/document-tile.c:219
msgid "Open in File Manager"
msgstr "Abrir no gerenciador de arquivos"

#. clean item from menu
#: ../libslab/document-tile.c:303
msgid "Remove from recent menu"
msgstr "Remover do menu recente"

#. clean all the items from menu
#: ../libslab/document-tile.c:311
msgid "Purge all the recent items"
msgstr "Eliminar todos os itens recentes"

#: ../libslab/document-tile.c:634
msgid "?"
msgstr "?"

#: ../libslab/document-tile.c:641
msgid "%l:%M %p"
msgstr "%k:%M"

#: ../libslab/document-tile.c:649
msgid "Today %l:%M %p"
msgstr "Hoje %I:%M %p"

#: ../libslab/document-tile.c:659
msgid "Yesterday %l:%M %p"
msgstr "Ontem %I:%M %p"

#: ../libslab/document-tile.c:671
msgid "%a %l:%M %p"
msgstr "%a %l:%M %p"

#: ../libslab/document-tile.c:679
msgid "%b %d %l:%M %p"
msgstr "%b %d %l:%M %p"

#: ../libslab/document-tile.c:681
msgid "%b %d %Y"
msgstr "%b %d %Y"

#: ../libslab/search-bar.c:255
msgid "Find Now"
msgstr "Localizar agora"

#: ../libslab/system-tile.c:126
#, c-format
msgid "Open %s"
msgstr "Abrir \"%s\""

#: ../libslab/system-tile.c:139
#, c-format
msgid "Remove from System Items"
msgstr "Remover dos itens de sistema"

#~ msgid "Application Browser"
#~ msgstr "Navegador de aplicações"

#~ msgid "Exit shell on add or remove action performed"
#~ msgstr "Sair do shell ao executar ação de adição ou remoção"

#~ msgid "Exit shell on help action performed"
#~ msgstr "Sair do shell ao executar ação de ajuda"

#~ msgid "Exit shell on start action performed"
#~ msgstr "Sair do shell ao executar ação de início"

#~ msgid "Exit shell on upgrade or uninstall action performed"
#~ msgstr "Sair do shell ao executar ação de atualização ou desinstalação"

#~ msgid "Filename of existing .desktop files"
#~ msgstr "Nome de arquivo dos arquivos .desktop existentes"

#~ msgid "Indicates whether to close the shell when a help action is performed"
#~ msgstr ""
#~ "Indica se o shell deve ser fechado quando uma ação de ajuda é executada"

#~ msgid ""
#~ "Indicates whether to close the shell when a start action is performed"
#~ msgstr ""
#~ "Indica se o shell deve ser fechado quando uma ação de início é executada"

#~ msgid ""
#~ "Indicates whether to close the shell when an add or remove action is "
#~ "performed"
#~ msgstr ""
#~ "Indica se o shell deve ser fechado quando uma ação de adição ou remoção é "
#~ "executada"

#~ msgid ""
#~ "Indicates whether to close the shell when an upgrade or uninstall action "
#~ "is performed"
#~ msgstr ""
#~ "Indica se o shell deve ser fechado quando uma ação de atualização ou "
#~ "desinstalação é executada"

#~ msgid "Max number of New Applications"
#~ msgstr "Número máximo de novas aplicações"

#~ msgid ""
#~ "The maximum number of applications that will be displayed in the New "
#~ "Applications category"
#~ msgstr ""
#~ "O número máximo de aplicações que serão mostradas na categoria novas "
#~ "aplicações"

#~ msgid "New Applications"
#~ msgstr "Novas aplicações"

#~ msgid "Filter"
#~ msgstr "Filtro"

#~ msgid "Groups"
#~ msgstr "Grupos"

#~ msgid "Application Actions"
#~ msgstr "Ações de aplicações"

#~ msgid "<b>Start %s</b>"
#~ msgstr "<b>Iniciar %s</b>"

#~ msgid ""
#~ "<span size=\"large\"><b>No matches found.</b> </span><span>\n"
#~ "\n"
#~ " Your filter \"<b>%s</b>\" does not match any items.</span>"
#~ msgstr ""
#~ "<span size=\"large\"><b>Nenhuma correspondência encontrada.</b> </"
#~ "span><span>\n"
#~ "\n"
#~ " Seu filtro \"<b>%s</b>\" não corresponde a nenhum item.</span>"

#~ msgid "Lock Screen"
#~ msgstr "_Travar tela..."

#~ msgid "Logout"
#~ msgstr "Sair"

#~ msgid "Shutdown"
#~ msgstr "Desligar"

#~ msgid "Unexpected attribute '%s' for element '%s'"
#~ msgstr "Atributo \"%s\" não esperado para o elemento \"%s\""

#~ msgid "Attribute '%s' of element '%s' not found"
#~ msgstr "Atributo \"%s\" do elemento \"%s\" não encontrado"

#~ msgid "Unexpected tag '%s', tag '%s' expected"
#~ msgstr "Marca \"%s\" não esperada, esperando marca \"%s\""

#~ msgid "Unexpected tag '%s' inside '%s'"
#~ msgstr "Marca \"%s\" não esperada dentro de \"%s\""

#~ msgid "No valid bookmark file found in data dirs"
#~ msgstr "Nenhum marcador válido localizado nos diretórios de dados"

#~ msgid "A bookmark for URI '%s' already exists"
#~ msgstr "Um marcador para a URI \"%s\" já existe"

#~ msgid "No bookmark found for URI '%s'"
#~ msgstr "Não foi encontrado marcador para a URI \"%s\""

#~ msgid "No MIME type defined in the bookmark for URI '%s'"
#~ msgstr "Tipo MIME não definido no marcador para a URI \"%s\""

#~ msgid "No private flag has been defined in bookmark for URI '%s'"
#~ msgstr ""
#~ "Nenhum sinalizador de privacidade foi definido no marcador para a URI \"%s"
#~ "\""

#~ msgid "No groups set in bookmark for URI '%s'"
#~ msgstr "Não foi configurado nenhum grupo para o marcador da URI \"%s\""

#~ msgid "No application with name '%s' registered a bookmark for '%s'"
#~ msgstr "Nenhuma aplicação com nome \"%s\" registrou um marcador para \"%s\""

#~ msgid "Default menu and application browser"
#~ msgstr "Navegador de aplicações e menu padrão"

#~ msgid "GNOME Main Menu"
#~ msgstr "Menu principal do GNOME"

#~ msgid "GNOME Main Menu Factory"
#~ msgstr "Fábrica do menu principal do GNOME"

#~ msgid "Main Menu"
#~ msgstr "Menu principal"

#~ msgid "_About"
#~ msgstr "_Sobre"

#~ msgid "_Open Menu"
#~ msgstr "Menu _abrir"

#~ msgid ".desktop file for the YaST2 network_devices utility"
#~ msgstr "arquivo .desktop para o utilitário de dispositivos_de_rede do YaST2"

#~ msgid ".desktop file for the file browser"
#~ msgstr "arquivo .desktop do navegador de arquivos"

#~ msgid ".desktop file for the gnome-system-monitor"
#~ msgstr "arquivo .desktop para o monitor de sistemas do gnome"

#~ msgid ".desktop file for the net config tool"
#~ msgstr "arquivo .desktop para a ferramenta de configuração de redes"

#~ msgid ".desktop path for the application browser"
#~ msgstr "caminho do arquivo .desktop do navegador de aplicações"

#~ msgid ""
#~ "This is the command to execute when the \"Open in File Manager\" menu "
#~ "item is activated."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando o item de menu \"Abrir no "
#~ "gerenciador de arquivos\" for ativado."

#~ msgid ""
#~ "This is the command to execute when the \"Open in File Manager\" menu "
#~ "item is activated. FILE_URI is replaced with a uri corresponding to the "
#~ "dirname of the activated file."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando o item de menu \"Abrir no "
#~ "gerenciador de arquivos\" for ativado. FILE_URI é substituído por um uri "
#~ "que corresponde ao nome de diretório do arquivo ativado."

#~ msgid ""
#~ "This is the command to execute when the \"Send To...\" menu item is "
#~ "activated."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando o item de menu \"Enviar "
#~ "para...\" for ativado."

#~ msgid ""
#~ "This is the command to execute when the \"Send To...\" menu item is "
#~ "activated. DIRNAME and BASENAME are replaced with the corresponding "
#~ "components of the activated tile."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando o item de menu \"Enviar "
#~ "para...\" for ativado. DIRNAME e BASENAME são substituídos pelos "
#~ "componentes correspondentes do bloco ativado."

#~ msgid "This is the command to execute when the search entry is used."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando a entrada da pesquisa for "
#~ "usada."

#~ msgid ""
#~ "This is the command to execute when the search entry is used. "
#~ "SEARCH_STRING is replaced with the entered search text."
#~ msgstr ""
#~ "Este é o comando que deve ser executado quando a entrada da pesquisa for "
#~ "usada. SEARCH_STRING é substituído pelo texto usado na pesquisa."

#~ msgid "command to uninstall packages"
#~ msgstr "comando para desinstalação de pacotes"

#~ msgid ""
#~ "command to uninstall packages, PACKAGE_NAME is replaced by the name of "
#~ "the package in the command"
#~ msgstr ""
#~ "comando para desinstalação de pacotes, PACKAGE_NAME é substituído pelo "
#~ "nome do pacote no comando"

#~ msgid "command to upgrade packages"
#~ msgstr "comando para atualização de pacotes"

#~ msgid ""
#~ "command to upgrade packages, PACKAGE_NAME is replaced by the name of the "
#~ "package in the command"
#~ msgstr ""
#~ "comando para atualização de pacotes, PACKAGE_NAME é substituído pelo nome "
#~ "do pacote no comando"

#~ msgid ""
#~ "contains the list (in no particular order) of allowable file tables to "
#~ "show in the file area. possible values: 0 - show the user-specified or "
#~ "\"Favorite\" applications table, 1 - show the recently used applications "
#~ "table, 2 - show the user-specified or \"Favorite\" documents table, 3 - "
#~ "show the recently used documents table, 4 - show the user-specified of "
#~ "\"Favorite\" directories or \"Places\" table, and 5 - show the recently "
#~ "used directories or \"Places\" table."
#~ msgstr ""
#~ "contém a lista (desordenada) das tabelas de arquivos com permissão de "
#~ "visualização na área de arquivos. valores permitidos: 0 - mostra a tabela "
#~ "das aplicações especificadas pelo usuário ou \"Favoritas\", 1 - mostra a "
#~ "tabela de aplicações recentemente usadas, 2 - mostra a tabela dos "
#~ "documentos especificados pelo usuário ou \"Favoritos\", 3 - mostra a "
#~ "tabela de documentos recentemente usados, 4 - mostra a tabela de "
#~ "diretórios especificados pelo usuário ou \"Favoritos\" ou a tabela de "
#~ "\"Locais\", e 5 - mostra os diretórios recentemente usados ou a tabela de "
#~ "\"Locais\"."

#~ msgid ""
#~ "contains the list of files (including .desktop files) to be excluded from "
#~ "the \"Recently Used Applications\" and \"Recent Files\" lists"
#~ msgstr ""
#~ "contém a lista dos arquivos (inclusive arquivos .desktop) que devem ser "
#~ "excluídos das listas \"Aplicações usadas recentemente\" e \"Arquivos "
#~ "recentes\""

#~ msgid "determines the limit of items in the file-area."
#~ msgstr "determina o limite dos itens na área de arquivos."

#~ msgid ""
#~ "determines the limit of items in the file-area. The number favorite items "
#~ "is not limited. This limit applies to the number of recent items, i.e. "
#~ "the number of recent items displayed is limited to max_total_items - the "
#~ "number of favorite items. If the number of favorite items exceeds "
#~ "max_total_items - min_recent_items than this limit is ignored."
#~ msgstr ""
#~ "determina o limite de itens na área de arquivos. O número de itens "
#~ "favoritos não é limitado. Esse limite aplica-se ao número de itens "
#~ "recentes, ou seja, o número de itens recentes apresentados é limitado a "
#~ "max_total_items - o número dos itens favoritos. Se o número de itens "
#~ "favoritos exceder o valor max_total_items - min_recent_items, então esse "
#~ "limite é ignorado."

#~ msgid ""
#~ "determines the minimum number of items in the \"recent\" section of the "
#~ "file-area."
#~ msgstr ""
#~ "determina o número mínimo de itens na área de arquivos \"recentes\"."

#~ msgid "determines which types of files to display in the file area"
#~ msgstr ""
#~ "determina quais tipos de arquivos devem ser mostrados na área de arquivos"

#~ msgid "if true, main menu is more anxious to close"
#~ msgstr "se verdadeiro, o menu principal fechará mais rápido"

#~ msgid ""
#~ "if true, main menu will close under these additional conditions: tile is "
#~ "activated, search activated"
#~ msgstr ""
#~ "se verdadeiro, o menu principal fechará caso as condições adicionais "
#~ "sejam atendidas: bloco ativado, pesquisa ativada"

#~ msgid "lock-down configuration of the file area"
#~ msgstr "configuração de bloqueio da área de arquivos"

#~ msgid "lock-down status for the application browser link"
#~ msgstr "estado do bloqueio do link do navegador de aplicações"

#~ msgid "lock-down status for the search area"
#~ msgstr "estado do bloqueio da área de pesquisa"

#~ msgid "lock-down status for the status area"
#~ msgstr "estado do bloqueio da área de estados"

#~ msgid "lock-down status for the system area"
#~ msgstr "estado do bloqueio da área do sistema"

#~ msgid "lock-down status for the user-specified apps section"
#~ msgstr "estado do bloqueio da área de aplicações do usuário"

#~ msgid "lock-down status for the user-specified dirs section"
#~ msgstr "estado do bloqueio da área de diretórios do usuário"

#~ msgid "lock-down status for the user-specified docs section"
#~ msgstr "estado do bloqueio da área de documentos do usuário"

#~ msgid "possible values = 0 [Applications], 1 [Documents], 2 [Places]"
#~ msgstr "valores permitidos = 0 [Aplicações], 1 [Documentos], 2 [Locais]"

#~ msgid ""
#~ "set to true if the link to the application browser should be visible and "
#~ "active."
#~ msgstr ""
#~ "definir como verdadeiro se o link para o navegador de aplicações deve "
#~ "estar visível e ativo."

#~ msgid "set to true if the search area should be visible and active."
#~ msgstr ""
#~ "definir como verdadeiro se a área de pesquisa deve estar visível e ativa."

#~ msgid "set to true if the status area should be visible and active."
#~ msgstr ""
#~ "definir como verdadeiro se a área de estados deve estar visível e ativa."

#~ msgid "set to true if the system area should be visible and active."
#~ msgstr ""
#~ "definir como verdadeiro se a área do sistema deve estar visível e ativa."

#~ msgid ""
#~ "set to true if the user is allowed to modify the list of system items."
#~ msgstr ""
#~ "definir como verdadeiro para permitir ao usuário modificar a lista de "
#~ "itens do sistema."

#~ msgid ""
#~ "set to true if the user is allowed to modify the list of user-specified "
#~ "or \"Favorite\" applications."
#~ msgstr ""
#~ "definir como verdadeiro para permitir ao usuário modificar a lista de "
#~ "aplicações \"Favoritas\"."

#~ msgid ""
#~ "set to true if the user is allowed to modify the list of user-specified "
#~ "or \"Favorite\" directories or \"Places\"."
#~ msgstr ""
#~ "definir como verdadeiro para permitir ao usuário modificar a lista de "
#~ "diretórios especificados pelo usuário ou \"Favoritos\" ou de \"Locais\"."

#~ msgid ""
#~ "set to true if the user is allowed to modify the list of user-specified "
#~ "or \"Favorite\" documents."
#~ msgstr ""
#~ "definir como verdadeiro para permitir ao usuário modificar a lista de "
#~ "documentos definidos pelo usuário ou de documentos \"Favoritos\"."

#~ msgid "_System Monitor"
#~ msgstr "Monitor do _sistema"

#~ msgid "%.1fG"
#~ msgstr "%.1fG"

#~ msgid "%.1fM"
#~ msgstr "%.1fM"

#~ msgid "%.1fK"
#~ msgstr "%.1fK"

#~ msgid "%.1fb"
#~ msgstr "%.1fb"

#~ msgid "Home: %s Free / %s"
#~ msgstr "Pasta pessoal: %s Disponível / %s"

#~ msgid "gnome-lockscreen"
#~ msgstr "gnome-lockscreen"

#~ msgid "The GNOME Main Menu"
#~ msgstr "O menu principal do GNOME"

#~ msgid "Network: None"
#~ msgstr "Rede: Nenhuma"

#~ msgid "Click to configure network"
#~ msgstr "Clique para configurar a rede"

#~ msgid "Networ_k: None"
#~ msgstr "_Rede: Nenhuma"

#~ msgid "Connected to: %s"
#~ msgstr "Conectado a: %s"

#~ msgid "Networ_k: Wireless"
#~ msgstr "_Rede: sem fios"

#~ msgid "Using ethernet (%s)"
#~ msgstr "Usando ethernet (%s)"

#~ msgid "Networ_k: Wired"
#~ msgstr "_Rede: com fios"

#~ msgid "Networ_k: GSM"
#~ msgstr "_Rede: GSM"

#~ msgid "Networ_k: CDMA"
#~ msgstr "_Rede: CDMA"

#~ msgid "Wireless Ethernet (%s)"
#~ msgstr "Rede sem fios (%s)"

#~ msgid "Wired Ethernet (%s)"
#~ msgstr "Rede com fios (%s)"

#~ msgid "Mobile Ethernet (%s)"
#~ msgstr "Rede móvel (%s)"

#~ msgid "Unknown"
#~ msgstr "Desconhecida"

#~ msgid "%d Mb/s"
#~ msgstr "%d Mb/s"

#~ msgid "Applications"
#~ msgstr "Aplicações"

#~ msgid "Computer"
#~ msgstr "Computador"

#~ msgid "Favorite Applications"
#~ msgstr "Aplicações favoritas"

#~ msgid "Favorite Documents"
#~ msgstr "Documentos favoritos"

#~ msgid "Favorite Places"
#~ msgstr "Locais favoritos"

#~ msgid "More Applications..."
#~ msgstr "Mais _aplicações..."

#~ msgid "More Documents..."
#~ msgstr "Mais _documentos..."

#~ msgid "More Places..."
#~ msgstr "Mais _locais..."

#~ msgid "Places"
#~ msgstr "Locais"

#~ msgid "Recent Applications"
#~ msgstr "Aplicações usadas recentemente"

#~ msgid "Recent Documents"
#~ msgstr "Documentos recentes"

#~ msgid "Search:"
#~ msgstr "Pesquisar:"

#~ msgid "System"
#~ msgstr "Sistema"

#~ msgid "Add the current launcher to favorites"
#~ msgstr "Adicione o lançador atual aos favoritos"

#~ msgid "Add the current document to favorites"
#~ msgstr "Adicione o documento atual aos favoritos"

#~ msgid "Remove the current document from favorites"
#~ msgstr "Remova o documento atual dos favoritos"
